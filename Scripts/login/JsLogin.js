﻿$(function() {
    //$(".dropdown").hover(
    //    function () {
    //        $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
    //        $(this).toggleClass('open');
    //        $('b', this).toggleClass("caret caret-up");
    //    },
    //    function () {
    //        $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
    //        $(this).toggleClass('open');
    //        $('b', this).toggleClass("caret caret-up");
    //    });

    $('#linkCargas').click(function() {
        debugger;
        $('#linkMenuPrin').parent('li').addClass('dropdown open');
        $('#linkMenuPrin').attr('aria-expanded', 'true');
    });

});

$(document).ready(function() {
    $("#btnEntrar").click(function() {
        var user = $("#txbNombreUsuario").val();
        var pass = $("#txbPassword").val();
        if (user != "" && pass != "") {
            $.ajax({
                url: 'http://localhost:9091/seidor-api/login',
                type: "POST",
                dataType: "json",
                data: {
                    username: user,
                    password: pass
                },
                success: function(respuesta, statusText, xhr) {
                    console.log(respuesta);
                    var status = xhr.status;
                    var head = xhr.getAllResponseHeaders();
                    console.log(status);
                    console.log(head);
                    if (status == 200) {
						sessionStorage.setItem('username', user);
                        location.href = "menuPrincipal.html";
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status === 401) {
						swal("Error!", "Error de autenticacion: username o password incorrecto", "error");
                    } else if (jqXHR.status === 0) {
						
						swal("Error!", "Not connect: Verify Network.", "error");

                    } else if (jqXHR.status == 404) {

						swal("Error!", "Requested page not found [404]", "error");

                    } else if (jqXHR.status == 500) {

						swal("Error!", "Internal Server Error [500].", "error");

                    } else if (textStatus === 'parsererror') {

						swal("Error!", "Requested JSON parse failed.", "error");

                    } else if (textStatus === 'timeout') {

						swal("Error!", "Time out error.", "error");

                    } else if (textStatus === 'abort') {

						swal("Error!", "Ajax request aborted.", "error");

                    } else {

						swal("Error!", "Uncaught Error: ", "error");

                    }
                    console.log("No se ha podido obtener la información");
                }
            });
        }

    });
});